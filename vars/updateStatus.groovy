#!/usr/bin/env groovy

/**
 * Post status to gitlab commit - handle check if commit is failed or pending before updating
 */

def call(String buildStatus = 'pending') {
  def postStatus = new postStatus();

  postStatus.withResult(buildStatus, env.GITLAB_TOKEN, env.GIT_COMMIT, env.BUILD_URL, env.JOB_NAME)
}
// println lastCommitResult('oyFq5Hbh7Y9Tf2-yTWGi', "272ffd774e2daf805569dc2d119e329e9da5dfa9")
