import groovy.json.JsonSlurper

def withResult(String state, String token, String commit, String targetUrl, String jobName) {
  try {
    def connection = new URL( "https://gitlab.com/api/v4/projects/2649592/statuses/${commit}?state=${state}&target_url=${targetUrl}&name=${jobName}").openConnection()
    connection.setRequestProperty('Accept', 'application/json')
    connection.setRequestProperty('Private-Token', token)
    connection.setRequestMethod("POST")
    connection.connect()

    if ( connection.responseCode == 201 ) {
        // execture POST command and get the JSON response
        def json = new JsonSlurper().parse(new InputStreamReader(connection.getInputStream(),"UTF-8"))
        print(json)

    } else {
        print("POST Status failed with: ${connection.responseCode}")
    }
  } catch (err) {
       print("ERROR:  ${err}")
  }
}
