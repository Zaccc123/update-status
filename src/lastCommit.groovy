import groovy.json.JsonSlurper

def result(String token, String commit) {
  try {
    def connection = new URL( "https://gitlab.com/api/v4/projects/2649592/repository/commits/${commit}/statuses?all=false").openConnection()

    connection.setRequestProperty('Accept', 'application/json')
    connection.setRequestProperty('Private-Token', token)
    connection.setRequestMethod("GET")
    connection.connect()

    if ( connection.responseCode == 200 ) {
        // execture GET command and get the JSON response
        def json = new JsonSlurper().parse(new InputStreamReader(connection.getInputStream(),"UTF-8"))
        connection.disconnect()

        // extract some data from the JSON, printing a report
        def lastCommitResult = json.last().status
        return lastCommitResult

    } else {
        return "No Status"
    }
  } catch (err) {
       print("ERROR:  ${err}")
       return "No Status"
  }
}

return this
